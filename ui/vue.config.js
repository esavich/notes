module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: 8081,
        proxy: {
            '/api': {
                // target: 'http://nginx:80/',
                target: 'http://docker.for.mac.localhost:3000',
                changeOrigin: true
            }
        }
    }
};