# notes-app
простой сервис заметок на php, vue.js, mysql

## Запуск
`docker-compose up`  запустит все необходимые сервисы, приложение будет доступно по адресу `http://localhost:8080/`
 (сначала будет произведена сборка образов, либо их можно предварительно загрузить с hub.docker.com с помощью команды `docker-compose pull`).

### Первый запуск
- измените если надо параметры подключения к БД в `api/config/config.php` 

- используйте `docker-compose run --rm php composer install` для установки зависимостей composer.

- используйте `docker-compose run --rm php php setupDb.php` для установки базы данных.

- используйте `docker-compose run --rm node yarn` для установки JS  зависимостей.

- используйте `docker-compose run --rm node yarn build` для создания готовой для production сборки.

## Разработка

- используйте `docker-compose run --rm php composer <команда>` для взаимодействия с composer.

- используйте `docker-compose run --rm -p 8081:8081 node yarn serve` для запуска dev-сервера vue с функцией горячей замены модулей и тд.

## Запуск без docker

- нужен nginx, php, mysql, а также nodejs (для сборки фронта) пример конфига nginx'а  тут - `docker/nginx/default.conf`, там ндо будет поменять адрес fastCGI сервера
 (параметр `fastcgi_pass`) и, вероятно, пути к папкам с фронтом и беком, также параметры подключения к БД в `api/config/config.php`.

- нужно установить php зависимости в папке api -  `composer install`

- установить js зависимости в папке ui - `yarn` 

- собрать фронт - `yarn build`

---
# In English
# notes-app
simple notes app built with php, vue.js, mysql.

## Running
use `docker-compose up` to run all services, app will be available at `http://localhost:8080/`.

### First launch
- change db connection params in `api/config/config.php`

- use `docker-compose run --rm php composer install` to install composer dependencies.

- use `docker-compose run --rm php php setupDb.php` to setup database.

- use `docker-compose run --rm node yarn` to install JS dependencies.

- use `docker-compose run --rm node yarn build` to produce a production-ready bundle. 



## Development

- use `docker-compose run --rm php composer <command>` to interact with composer.

- use `docker-compose run --rm -p 8081:8081 node yarn serve` to start vue dev server with HMR, live reload, etc. 

