<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/config.php';


$dsn = sprintf("mysql:host=%s", $container->get('dbHost'));
$user = $container->get('dbUser');
$password = $container->get('dbPass');
$pdo = new PDO($dsn, $user, $password);

$dbName = $container->get('dbName');

//prepared statements не работают для имен базы данных и таблиц
$pdo->query(sprintf('CREATE DATABASE IF NOT EXISTS `%s`', $dbName));
$pdo->query(sprintf('CREATE TABLE IF NOT EXISTS `%s`.`notes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  PRIMARY KEY (`id`));
', $dbName));
