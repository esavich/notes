<?php

use App\Controllers\NotesController;
use App\Services\NotesService;
use App\Services\NotesServiceInterface;
use League\Container\Container;

$container = new Container;

//параметры подключения к базе
$container->add('dbHost', 'mysql');
$container->add('dbName', 'notes');
$container->add('dbUser', 'root');
$container->add('dbPass', 'example');

$container->add(NotesServiceInterface::class, function () use ($container) {
    // mysql:dbname=notes;host=mysql
    $dsn = sprintf("mysql:dbname=%s;host=%s", $container->get('dbName'), $container->get('dbHost'));
    $user = $container->get('dbUser');
    $password = $container->get('dbPass');
    $pdo = new PDO($dsn, $user, $password);

    return new NotesService($pdo);
});

$container->add(NotesController::class)->addArgument(NotesServiceInterface::class);
