<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\Controllers\NotesController;
use League\Route\Http\Exception\NotFoundException;
use League\Route\RouteGroup;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

//создание Request из глобальных переменных
$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

require_once dirname(__DIR__) . '/config/config.php';

$strategy = (new League\Route\Strategy\ApplicationStrategy)->setContainer($container);
$router = (new League\Route\Router)->setStrategy($strategy);

//middleware для разбора параметров запроса
$router->middleware(new BodyParamsMiddleware());

//роутинг
$router->group('/api/notes', function (RouteGroup $route) {
    $route->map('GET', '/', [NotesController::class, 'indexAction']);
    $route->map('POST', '/', [NotesController::class, 'addAction']);
    $route->map('DELETE', '/{id}', [NotesController::class, 'deleteAction']);
});


try {
    $response = $router->dispatch($request);
} catch (NotFoundException $e) {
    //если не сматчился роут - возвращаем 404
    $response = new JsonResponse(['error' => 'route not found'], 404);
} catch (PDOException $e) {
    //ошибка базы данных
    $response = new JsonResponse(['error' => 'db error'], 500);
}

//посылаем ответ в браузер
(new SapiEmitter())->emit($response);
