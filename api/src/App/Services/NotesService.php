<?php

namespace App\Services;

use PDO;

class NotesService implements NotesServiceInterface
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * NotesService constructor.
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Возвращает список заметок
     *
     * @return array
     */
    public function get()
    {
        $result = $this->connection->query('SELECT * FROM notes');

        return $result ? $result->fetchAll(PDO::FETCH_ASSOC) : false;
    }

    /**
     * Вставляет заметку в базу, возвращает id  записи
     *
     * @param $text
     * @return int
     */
    public function add($text)
    {
        $stmt = $this->connection->prepare('INSERT INTO notes (text) VALUES (:text)');
        $stmt->execute(['text' => $text]);

        $lastInsertId = (int)$this->connection->lastInsertId();

        return $lastInsertId;
    }

    /**
     * Удаляет запись
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $stmt = $this->connection->prepare('DELETE FROM notes WHERE id = :id');
        $stmt->execute(['id' => $id]);

        return $stmt->rowCount() > 0;
    }
}
