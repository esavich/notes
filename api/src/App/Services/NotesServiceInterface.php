<?php

namespace App\Services;

interface NotesServiceInterface
{
    public function get();

    public function add($text);

    public function delete($id);
}
