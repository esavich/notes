<?php

namespace App\Controllers;

use App\Services\NotesServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class NotesController
{
    private $service;

    public function __construct(NotesServiceInterface $notesService)
    {
        $this->service = $notesService;
    }

    public function indexAction(): ResponseInterface
    {
        $notes = $this->service->get();

        if ($notes === false) {
            return new JsonResponse(['error' => 'Couldn\'t load notes'], 404);
        } else {
            return new JsonResponse($notes);
        }
    }

    public function addAction(ServerRequestInterface $request): ResponseInterface
    {
        $parsedBody = $request->getParsedBody();
        $text = $parsedBody['text'];
        $newId = $this->service->add($text);

        if ($newId) {
            return new JsonResponse(['id' => $newId, 'text' => $text]);
        } else {
            return new JsonResponse(['error' => 'Couldn\'t add note'], 500);
        }
    }

    public function deleteAction(ServerRequestInterface $request, array $args): ResponseInterface
    {
        $id = (int)$args['id'];
        $success = $this->service->delete($id);

        if ($success) {
            return new JsonResponse(['id' => $id]);
        } else {
            return new JsonResponse(['error' => 'Note not found'], 404);
        }
    }
}
